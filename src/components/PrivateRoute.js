import { Navigate } from "react-router";



const PrivateRoute = ({children}) => {
    const islogin = localStorage.getItem('itemName')
    return islogin ? children : <Navigate to="/"/>
        
        
    
};

export default PrivateRoute;