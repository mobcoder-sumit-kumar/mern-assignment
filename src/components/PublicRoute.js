import { Navigate } from "react-router";



const PublicRoute = ({children}) => {
    const islogin = localStorage.getItem('itemName')   
    return (
        islogin ? <Navigate to= "/myboard" /> : children
        
    );
};

export default PublicRoute;