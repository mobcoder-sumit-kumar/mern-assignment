import React from 'react'
import { Container } from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button'
import { useNavigate } from 'react-router';
// import { useDispatch } from 'react-redux';
// import { setlogout } from '../Redux/Action/action';

function NavBar() {
    // const navigate = useNavigate()
    // const dispatch = useDispatch()
    const token = localStorage.getItem('itemName')
   const handlelogout = () => {
       localStorage.removeItem('itemName')
    //    navigate('/')
    //    dispatch(setlogout())
   }
    return (
        <div>
            <Navbar bg="dark" variant="dark" >
    <Container>
    <Navbar.Brand >Lendo</Navbar.Brand>
    { token ?
    <Button variant="outline-info" onClick={() => handlelogout()}>Logout</Button> : ""}
    </Container>
  </Navbar>
        </div>
    )
}

export default NavBar
