import React,{ useState} from 'react'
import { Card, Modal } from 'react-bootstrap'
import './Myboard.css'
import Button from 'react-bootstrap/Button'
import axios from 'axios';
function Myboard() {
   
    const [show, setShow] = useState(false);
    const [data, setdata] = useState();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
     const handleSubmit = () => {
       setShow(false);
      axios.post('');
     }    
     const handledata = (e) =>{
      setdata(e.target.value);
     }

    return (
        
        <div className="container">
            <Button variant="outline-success" id="newbtn"onClick={() => handleShow()}>Create new board</Button>
            <Modal show={show} onHide={handleClose}  >
        <Modal.Header closeButton>
          <label>Create new Board</label>
        </Modal.Header>
        <input placeholder="Board Name" onChange={(e) => handledata(e)}/>
          <label id="chooseboard">Choose board color</label>
          <div id="color">
          <button class="cbtn" id="btn1"></button> <button class="cbtn" id="btn2"></button>
         
          <button class="cbtn" id="btn3"></button><button class="cbtn" id="btn4"></button><button class="cbtn" id="btn5"></button><button class="cbtn" id="btn6"></button><button class="cbtn" id="btn7"></button>
          </div>
        
          
          <Button variant="primary" onClick={handleClose}>
            CREATE
          </Button>
        
      </Modal>
            {/* <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        // contentLabel="Example Modal"
      >
        <h2 >Create new board</h2>
        <button onClick={closeModal}>close</button>
        
        <form>
          
        </form>
      </Modal> */}
            <hr></hr>
            <label>My Boards</label>
            <Card
    // bg={variant.toLowerCase()}
    // key={idx}
    // text={variant.toLowerCase() === 'light' ? 'dark' : 'white'}
    style={{ width: '18rem' }}
    // className="mb-2"
  >
    <Card.Header>Header</Card.Header>
    <Card.Body>
      <Card.Title> Card Title </Card.Title>
      {/* <Card.Text>
        Some quick example text to build on the card title and make up the bulk
        of the card's content.
      </Card.Text> */}
    </Card.Body>
  </Card>
        </div>
    )
}

export default Myboard
