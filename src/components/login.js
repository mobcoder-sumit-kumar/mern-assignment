import React, {useState, useEffect} from 'react'

import { useNavigate } from 'react-router'
import './login.css'
function Login() {
    const [email, setemail] = useState()
    const [password, setpassword] = useState()
    const [show, setshow] = useState(false)
    const [Input, setInput] = useState({
        email: "",
        password: ""
    })
    const navigate= useNavigate();

  const handleClick = async() => {
        setshow(false);
     setInput({email: email, password: password})
     navigate('/myboard')
     localStorage.setItem('itemName', "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0NDc3MiIsImlhdCI6MTYzNzg0MzIyN30.onWLAJHqRqtW0-NUtqlKd_uNjLUy2rkY--Fns-o_39Un5KxOb0lWdor4VGvuitHoXDmLnIMEVOHggHIy4K9uvA")   
  }

//    const fetch_api = async(props) => {
    
//         localStorage.setItem('itemName', "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0NDc3MiIsImlhdCI6MTYzNzg0MzIyN30.onWLAJHqRqtW0-NUtqlKd_uNjLUy2rkY--Fns-o_39Un5KxOb0lWdor4VGvuitHoXDmLnIMEVOHggHIy4K9uvA")
//         navigate('/myboard')
       
//   }

//   useEffect(() => {
//       fetch_api(Input)
//   }, [Input])

    return (
        <div> 
        <div className="main">
            
            <div className="container1">
                <h3>Login</h3>
                <input className="input-box" name="email" type="email" placeholder="email" onChange={ (e) => setemail(e.target.value)}/> <br/>
                <input className="input-box" name="password" type="password" placeholder="password" onChange={ (e) => setpassword(e.target.value)}/> <br/>
                {show ? <label>Check Email and Password</label>: ""}<br/>
                <button onClick={()=>handleClick()}>LogIn</button>
                <label >Create a new account</label>
            </div>            
        </div></div>
    )
}

export default Login
