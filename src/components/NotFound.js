import React from 'react'

function NotFound() {
    return (
        <div>
            <h3>Not Found</h3>
            <p>The requested URL not found on this server</p>
        </div>
    )
}

export default NotFound
