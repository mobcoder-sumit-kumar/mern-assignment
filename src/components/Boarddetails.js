import React, {useState, useEffect} from 'react'
import { AiTwotoneHome } from 'react-icons/ai';
import './Boarddetails.css'
function Boarddetails() {

    const _taskList = [
        {id: 1, taskName: 'task 1', status: 1},
        {id: 2, taskName: 'task 2', status: 1},
        {id: 3, taskName: 'task 3', status: 2},
        {id: 4, taskName: 'task 4', status: 2},
        {id: 5, taskName: 'task 5', status: 4},
        {id: 6, taskName: 'task 6', status: 1},
        {id: 7, taskName: 'task 7', status: 2},
        {id: 8, taskName: 'task 8', status: 1},
        {id: 9, taskName: 'task 9', status: 4},
        {id: 10, taskName: 'task 10', status: 1},
        {id: 11, taskName: 'task 11', status: 2},
        {id: 12, taskName: 'task 12', status: 5},
        {id: 13, taskName: 'task 13', status: 5},
        {id: 14, taskName: 'task 14', status: 5},
        {id: 15, taskName: 'task 15', status: 2},
        {id: 16, taskName: 'task 16', status: 3},
        {id: 17, taskName: 'task 17', status: 2},
        {id: 18, taskName: 'task 18', status: 2},
        {id: 19, taskName: 'task 19', status: 1},
        {id: 20, taskName: 'task 20', status: 1}
      ]

  const [Alltask, setAlltask] = useState(_taskList);
  const [ToDo, setToDo] = useState([]);
  const [inProgress, setinProgress] = useState([]);
  const [onHold, setOnHold] = useState([]);
  const [completed, setCompleted] = useState([]);
  const [released, setReleased] = useState([]);
  const [dragTask, setDragTask] = useState();


  useEffect(() => {
    let todofilter = Alltask.filter(i => i.status == 1);
    let inProgressfilter = Alltask.filter(i => i.status == 2);
    let onHoldfilter = Alltask.filter(i => i.status == 3);
    let completedfilter = Alltask.filter(i => i.status == 4);
    let releasedfilter = Alltask.filter(i => i.status == 5);

    setToDo(todofilter);
    setinProgress(inProgressfilter);
    setOnHold(onHoldfilter);
    setCompleted(completedfilter);
    setReleased(releasedfilter);
    setDragTask();
  }, [Alltask])

   const onDragStart = (e, item) => {
    setDragTask(item);
  }

  const onDragOver = e => {
    e.preventDefault();
  }

  const onDrop = (e, cat) => {
    let tempTasks = Alltask.filter(i => i.id != dragTask.id);
    let drag = {...dragTask, status: cat};
    tempTasks.push(drag);
    setAlltask(tempTasks);
  }

    return (
        <div className="main">
            <div className="head"><div>
            <AiTwotoneHome /></div> <div>Board Name</div>
            </div>
            <div className="outer">
            <div className="outer_1">
            <h5>To do</h5>
            <div className="lists"
              onDragOver={(e)=>onDragOver(e)}
              onDrop={e => onDrop(e, 1)}>
                {ToDo.map(option => <div id={option.id} onDragStart={(e) => onDragStart(e, option)} draggable className="drag" key={option.id}>{option.taskName}</div>)}
            </div>
          </div>


          <div className="outer_1">
            <h5>In Progress</h5>
            <div className="lists"
              onDragOver={(e)=>onDragOver(e)}
              onDrop={e=> onDrop(e, 2)}>
                {inProgress.map(option => <div onDragStart={(e) => onDragStart(e , option)} draggable className="drag" key={option.id}>{option.taskName}</div>)}
            </div>
          </div>


          <div className="outer_1">
          <h5>On hold</h5>
            <div className="lists"
              onDragOver={(e)=>onDragOver(e)}
              onDrop={e=> onDrop(e, 3)}>
                {onHold.map(option => <div onDragStart={(e) => onDragStart(e, option)} draggable className="drag"key={option.id}>{option.taskName}</div>)}
            </div>
          </div>

          <div className="outer_1">
          <h5>Completed</h5>
            <div className="lists"
              onDragOver={(e)=>onDragOver(e)}
              onDrop={e=> onDrop(e, 4)}>
                {completed.map(option => <div onDragStart={(e) => onDragStart(e, option)} draggable className="drag" key={option.id}>{option.taskName}</div>)}
            </div>
          </div>

          <div className="outer_1">
          <h5>Completed</h5>
            <div className="lists"
              onDragOver={(e)=>onDragOver(e)}
              onDrop={e=> onDrop(e, 5)}>
                {released.map(option => <div onDragStart={(e) => onDragStart(e, option)} draggable className="drag" key={option.id}>{option.taskName}</div>)}
            </div>
          </div>



          </div>   
        </div>
    )
}

export default Boarddetails
