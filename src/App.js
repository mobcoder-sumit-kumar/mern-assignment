import './App.css';
import {BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Boarddetails from './components/Boarddetails';
import Login from './components/login';
import Myboard from './components/Myboards';
import Signup from './components/signup';
import PublicRoute from './components/PublicRoute';
import PrivateRoute from './components/PrivateRoute';
import { NotFound } from 'http-errors';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path="/" element={<PublicRoute><Signup/></PublicRoute>}/>
          <Route exact path="/login" element={<PublicRoute><Login/></PublicRoute>}/>
          <Route exact path="/myboard" element={<PrivateRoute><Myboard/></PrivateRoute>} />
          <Route exact path= "myboard/boardDetails" element={<PrivateRoute><Boarddetails/></PrivateRoute>}/>
          {/* <Route path="*" element={<PrivateRoute><NotFound/></PrivateRoute>} /> */}
        </Routes>
      </Router>
        {/* <NavBar/> */}
        {/* <Login/> */}
        {/* <Signup/> */}
        {/* <Myboard/> */}
        {/* // <Boarddetails/> */}
      
    </div>
  );
}

export default App;
